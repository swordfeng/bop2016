Xid = Fid | Cid | Jid | Auid

(id, id)
---
id -> id (1)  
id -> id -> id (1)  
id -> Xid -> id (2)  
id -> id -> id -> id (1)  
id -> id -> Xid -> id (2)  
id -> Xid -> id -> id (3)

(id, Auid)
---
id -> Auid (4)  
id -> id -> Auid (4)  
id -> id -> id -> Auid (4)  
id -> Xid -> id -> Auid (5)  
id -> Auid -> Afid -> Auid (6)

(Auid, id)
---
Auid -> id (7)  
Auid -> id -> id (7)  
Auid -> id -> id -> id (7)  
Auid -> id -> Xid -> id (7)  
Auid -> Afid -> Auid -> id (9)

(Auid, Auid)
---
Auid -> Afid -> Auid (10)  
Auid -> id -> Auid (11)  
Auid -> id -> id -> Auid (11)  
