'use strict';
var request = require('request');
var co = require('co');
var https = require('https');
var querystring = require('querystring');

function req(id1, id2) {
    return new Promise((resolve, reject) => {
        request(`http://bop-naive.chinacloudapp.cn/?id1=${id1}&id2=${id2}`, function (error, response, body) {
            if (error) reject(error);
            resolve(JSON.parse(body));
        });
    });
}

function makequery(expr) {
    var obj = {};
    obj['expr'] = expr;
    obj['subscription-key'] = 'f7cc29509a8443c5b3a5e56b0e38b5a6';
    obj['attributes'] = 'Id';
    if (!obj['count']) obj['count'] = 1;
    let url = 'https://oxfordhk.azure-api.net/academic/v1.0/evaluate?'
            + querystring.stringify(obj);
    return new Promise((resolve, reject) => {
        https.get(url, (res) => {
            let retbuf = Buffer(0);
            res.on('data', (buf) => {
                retbuf = Buffer.concat([retbuf, buf]);
            });
            res.on('end', () => {
                resolve(JSON.parse(retbuf.toString('utf8')).entities);
            });
            res.on('error', (e) => reject(e));
        }).on('error', (e) => reject(e));
    });
}

var checkpath = co.wrap(function *(id1, id2) {
    var r = [];
    r.push(makequery(`And(Id=${id1}, RId=${id2})`));
    r.push(makequery(`And(Id=${id1}, Composite(F.FId=${id2}))`));
    r.push(makequery(`And(Id=${id2}, Composite(F.FId=${id1}))`));
    r.push(makequery(`And(Id=${id1}, Composite(C.CId=${id2}))`));
    r.push(makequery(`And(Id=${id2}, Composite(C.CId=${id1}))`));
    r.push(makequery(`And(Id=${id1}, Composite(J.JId=${id2}))`));
    r.push(makequery(`And(Id=${id2}, Composite(J.JId=${id1}))`));
    r.push(makequery(`Composite(And(AA.AuId=${id1}, AA.AfId=${id2}))`));
    r.push(makequery(`Composite(And(AA.AuId=${id2}, AA.AfId=${id1}))`));
    r.push(makequery(`And(Id=${id1}, Composite(AA.AuId=${id2}))`));
    r.push(makequery(`And(Id=${id2}, Composite(AA.AuId=${id1}))`));
    var valid = false;
    for (var p of r) {
        var t = yield p;
        if (t.length > 0) {
            valid = true;
            break;
        }
    }
    return valid;
});

var test = co.wrap(function *test(id1, id2, path) {
    var res = yield req(id1, id2);
    if (path) {
        // check if path is in res
        var found = false;
        for (let p of res) {
            if (p.length !== path.length) continue;
            var i;
            for (i = 0; i < p.length; i++) {
                if (p[i] !== path[i]) break;
            }
            if (i === p.length) {
                found = true;
                break;
            }
        }
        if (found) console.log('path found');
        else console.error('PATH NOT FOUND!!!');
    }
    // check if all paths are valid
    var checkers = [];
    for (let p of res) checkers.push(co(function *(){
        for (var i = 0; i < p.length - 1; i++) {
            var t = yield checkpath(p[i], p[i + 1]);
            if (!t) {
                console.error('INVALID PATH:', p);
                break;
            }
        }
    }));
    yield Promise.all(checkers);
    console.log(`${res.length} paths found`);
    return res;
});

co(function *() {
    yield test(2293411398, 2092422745, [2293411398,2014717030,1974777215,2092422745]);
    yield test(2251253715, 2180737804);
    yield test(2147152072, 189831743);
    yield test(2332023333, 2310280492);
    yield test(2332023333, 57898110);
    yield test(57898110, 2014261844);
});

process.on('uncaughtException', (e) => console.error(e.stack));
process.on('unhandledRejection', (e) => console.error(e.stack));
