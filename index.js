'use strict';
var co = require('co');
var koa = require('koa');
var json = require('koa-json');
var https = require('https');
var querystring = require('querystring');
var fs = require('fs');
var util = require('util');

var app = koa();
var querytime = 0, querysent = 0, querycount = 0;
var reqnum = 0;
var requestedin30s = false;

var logfile = fs.createWriteStream('log.txt', {
    flags: 'a'
});

function log() {
    let args = Array.prototype.slice.apply(arguments);
    let str = args.map(x => typeof x === 'string' ? x : util.inspect(x)).join(' ');
    console.log(str);
    logfile.write(str);
    logfile.write('\n');
}

app.use(json({pretty: false}));

app.use(function *(next) {
    requestedin30s = true;
    // WARNING: Node.js have only precision of 52bits!!!
    let id1 = parseInt(this.request.query.id1);
    let id2 = parseInt(this.request.query.id2);
    log(`request: id1 = ${id1}, id2 = ${id2}`);
    log('querystring:', this.request.querystring);
    if (Number.isNaN(id1)) return;

    reqnum++;
    let active = true;
    let results = [];
    let startTime = new Date;
    querytime = 0, querysent = 0, querycount = 0;

    function addResult(path, info) {
        if (!active) return false;
        log('new result:', path);
        if (info) log('with info:', info);
        log('at:', (new Date - startTime) / 1000, 's');
        results.push(path);
    }

    // do requests!
    yield Promise.race([
        Promise.all([
            proc1(id1, id2, addResult),
            proc2(id1, id2, addResult),
            proc3(id1, id2, addResult),
            proc4(id1, id2, addResult),
            proc5(id1, id2, addResult),
            proc6(id1, id2, addResult),
            proc7(id1, id2, addResult),
            proc9(id1, id2, addResult),
            proc10(id1, id2, addResult),
            proc11(id1, id2, addResult),
        ]),
        timeouter(5000)
    ]);

    active = false;
    this.body = results;
    log('response at:', (new Date - startTime) / 1000, 's');
    log('result count:', results.length);
    log('average query time:', querytime / querycount, 'ms');
    log('query sent:', querysent, 'recv:', querycount);
});

app.listen(3000);


var querycache = {};
function makequery(obj) {
    let cur = reqnum;
    let hash = querystring.stringify(obj);
    if (querycache[hash]) return querycache[hash];
    //log('query', obj.expr);
    let startTime = new Date;
    querysent++;
    obj['subscription-key'] = 'f7cc29509a8443c5b3a5e56b0e38b5a6';
    obj['attributes'] = 'Id,AA.AuId,AA.AfId,RId,F.FId,J.JId,C.CId';
    if (!obj['count']) obj['count'] = 1000;
    let url = 'https://oxfordhk.azure-api.net/academic/v1.0/evaluate?'
            + querystring.stringify(obj);
    querycache[hash] = new Promise((resolve, reject) => {
        let resolved = false;
        function dorequest() {
            https.get(url, (res) => {
                let retbuf = Buffer(0);
                res.on('data', (buf) => {
                    retbuf = Buffer.concat([retbuf, buf]);
                });
                res.on('end', () => {
                    if (reqnum !== cur) return;
                    querytime += new Date - startTime;
                    querycount++;
                    if (!resolved) {
                        resolve(JSON.parse(retbuf.toString('utf8')).entities);
                        resolved = true;
                    }
                });
                res.on('error', (e) => reject(e));
            }).on('error', (e) => reject(e));
        }
        dorequest();
        setTimeout(() => {
            if (!resolved) dorequest();
        }, 800);
    });
    return querycache[hash];
}

var idcache = {};
function queryid(id) {
    if (idcache[id] === undefined) {
        idcache[id] = makequery({
            expr: `Id=${id}`
        });
    }
    return idcache[id];
}

var afcache = {};
function queryaf(auid) {
    if (afcache[auid] === undefined) {
        afcache[auid] = co(function *() {
            let res = yield querybyauid(auid);
            let ret = [];
            let afids = {};
            for (let item of res) {
                if (item.AA) for (let aa of item.AA) {
                    if (aa.AuId !== auid) continue;
                    if (!aa.AfId) continue;
                    if (afids[aa.AfId]) continue;
                    afids[aa.AfId] = true;
                    ret.push(aa.AfId);
                }
            }
            return ret;
        });
    }
    return afcache[auid];
}

function querybyauid(auid) {
    return makequery({
        expr: `Composite(AA.AuId=${auid})`
    });
}

function timeouter(time) {
    return new Promise((resolve) => {
        setTimeout(resolve, time);
    });
}

// clear cache when no requests in 30s
setInterval(() => {
    if (!requestedin30s) {
        querycache = {};
        idcache = {};
        afcache = {};
    }
    requestedin30s = false;
}, 30000);

process.on('uncaughtException', (e) => console.error(e.stack));
process.on('unhandledRejection', (e) => console.error(e.stack));


function proc1(id1, id2, addResult) {
    return co(function *() {
        let idl1 = yield queryid(id1);
        if (idl1.length === 0) return;
        let ts = [];
        for (let ridl1 of idl1[0].RId) {
            if (ridl1 === id2) {
                addResult([id1, ridl1]);
                continue;
            }
            ts.push(co(function *() {
                let idl2 = yield queryid(ridl1);
                let ts = [];
                for (let iditem of idl2) {
                    for (let ridl2 of iditem.RId) {
                        if (ridl2 === id2) {
                            addResult([id1, ridl1, ridl2], 'id,id,id');
                            continue;
                        }
                        ts.push(co(function *() {
                            let idl3 = yield queryid(ridl2);
                            for (let iditem of idl3) {
                                for (let ridl3 of iditem.RId) {
                                    if (ridl3 === id2) {
                                        addResult([id1, ridl1, ridl2, ridl3], 'id,id,id,id');
                                        break;
                                    }
                                }
                            }
                        }));
                    }
                }
                yield Promise.all(ts);
            }));
        }
        yield Promise.all(ts);
    });
}

function proc2(id1, id2, addResult) {
    return co(function *() {
        let ts = [];
        let id1item = queryid(id1);
        let id2item = queryid(id2);
        id1item = yield id1item;
        id2item = yield id2item;
        if (id1item.length === 0) return;
        if (id2item.length === 0) return;
        id1item = id1item[0];
        id2item = id2item[0];
        if (id1item.C !== undefined && id2item.C !== undefined && id1item.C.CId === id2item.C.CId) {
            addResult([id1, id1item.C.CId, id2], 'id,cid,id');
        }
        if (id1item.J !== undefined && id2item.J !== undefined && id1item.J.JId === id2item.J.JId) {
            addResult([id1, id1item.J.JId, id2], 'id,jid,id');
        }
        if (id1item.F !== undefined && id2item.F !== undefined) {
            let fids = {};
            for (let fid of id1item.F) fids[fid.FId] = true;
            for (let fid of id2item.F) {
                if (fids[fid.FId]) {
                    addResult([id1, fid.FId, id2], 'id,fid,id');
                }
            }
        }
        if (id1item.AA !== undefined && id2item.AA !== undefined) {
            let auids = {};
            for (let auid of id1item.AA) auids[auid.AuId] = true;
            for (let auid of id2item.AA) {
                if (auids[auid.AuId]) {
                    addResult([id1, auid.AuId, id2], 'id,auid,id');
                }
            }
        }
        for (let id3 of id1item.RId) ts.push(co(function *() {
            let id3item = yield queryid(id3);
            id3item = id3item[0];
            if (id3item.C !== undefined && id2item.C !== undefined && id3item.C.CId === id2item.C.CId) {
                addResult([id1, id3, id3item.C.CId, id2], 'id,id,cid,id');
            }
            if (id3item.J !== undefined && id2item.J !== undefined && id3item.J.JId === id2item.J.JId) {
                addResult([id1, id3, id3item.J.JId, id2], 'id,id,jid,id');
            }
            if (id3item.F !== undefined && id2item.F !== undefined) {
                let fids = {};
                for (let fid of id3item.F) fids[fid.FId] = true;
                for (let fid of id2item.F) {
                    if (fids[fid.FId]) {
                        addResult([id1, id3, fid.FId, id2], 'id,id,fid,id');
                    }
                }
            }
            if (id3item.AA !== undefined && id2item.AA !== undefined) {
                let auids = {};
                for (let auid of id3item.AA) auids[auid.AuId] = true;
                for (let auid of id2item.AA) {
                    if (auids[auid.AuId]) {
                        addResult([id1, id3, auid.AuId, id2], 'id,id,auid,id');
                    }
                }
            }
        }));
        yield Promise.all(ts);
    });
}

function proc3(id1, id2, addResult) {
    return co(function *() {
        let ts = [];
        let id1item = queryid(id1);
        let id2item = queryid(id2);
        id1item = yield id1item;
        id2item = yield id2item;
        if (id1item.length === 0) return;
        if (id2item.length === 0) return;
        id1item = id1item[0];
        id2item = id2item[0];
        ts.push(co(function *() {
            if (!id1item.C) return;
            let res = yield makequery({
                expr: `And(Composite(C.CId=${id1item.C.CId}), RId=${id2})`
            });
            for (let item of res) {
                addResult([id1, id1item.C.CId, item.Id, id2], 'id,cid,id,id');
            }
        }));
        ts.push(co(function *() {
            if (!id1item.J) return;
            let res = yield makequery({
                expr: `And(Composite(J.JId=${id1item.J.JId}), RId=${id2})`
            });
            for (let item of res) {
                addResult([id1, id1item.J.JId, item.Id, id2], 'id,jid,id,id');
            }
        }));
        ts.push(co(function *() {
            if (!id1item.F) return;
            let ts = [];
            for (let fids of id1item.F) ts.push(co(function *() {
                let res = yield makequery({
                    expr: `And(Composite(F.FId=${fids.FId}), RId=${id2})`
                });
                for (let item of res) {
                    addResult([id1, fids.FId, item.Id, id2], 'id,fid,id,id');
                }
            }));
            yield Promise.all(ts);
        }));
        ts.push(co(function *() {
            if (!id1item.AA) return;
            let ts = [];
            for (let auids of id1item.AA) ts.push(co(function *() {
                let res = yield makequery({
                    expr: `And(Composite(AA.AuId=${auids.AuId}), RId=${id2})`
                });
                for (let item of res) {
                    addResult([id1, auids.AuId, item.Id, id2], 'id,auid,id,id');
                }
            }));
            yield Promise.all(ts);
        }));
        yield Promise.all(ts);
    });
}

function proc4(id1, id2, addResult) {
    return co(function *() {
        let ts = [];
        let idl1 = yield queryid(id1);
        if (idl1.length === 0) return;
        let id1item = idl1[0];
        if (id1item.AA) {
            for (let auid of id1item.AA) {
                if (auid.AuId === id2) {
                    addResult([id1, id2], 'id,auid');
                    break;
                }
            }
        }
        if (!id1item.RId) return;
        for (let id3 of id1item.RId) ts.push(co(function *() {
            let ts = [];
            let id3item = yield queryid(id3);
            if (id3item.length === 0) return;
            id3item = id3item[0];
            if (id3item.AA) {
                for (let auid of id3item.AA) {
                    if (auid.AuId === id2) {
                        addResult([id1, id3, id2], 'id,id,auid');
                        break;
                    }
                }
            }
            if (!id3item.RId) return;
            for (let id4 of id3item.RId) ts.push(co(function *() {
                let id4item = yield queryid(id4);
                if (id4item.length === 0) return;
                id4item = id4item[0];
                if (id4item.AA) {
                    for (let auid of id4item.AA) {
                        if (auid.AuId === id2) {
                            addResult([id1, id3, id4, id2], 'id,id,id,auid');
                            break;
                        }
                    }
                }
            }));
            yield Promise.all(ts);
        }));
        yield Promise.all(ts);
    });
}

function proc5(id1, id2, addResult) {
    return co(function *() {
        let ts = [];
        let idl1 = yield queryid(id1);
        if (idl1.length === 0) return;
        let id1item = idl1[0];
        if (id1item.C) {
            ts.push(co(function *() {
                let res = yield makequery({
                    expr: `And(Composite(C.CId=${id1item.C.CId}), Composite(AA.AuId=${id2}))`
                });
                for (let item of res) {
                    addResult([id1, id1item.C.CId, item.Id, id2], 'id,cid,id,auid');
                }
            }));
        }
        if (id1item.J) {
            ts.push(co(function *() {
                let res = yield makequery({
                    expr: `And(Composite(J.JId=${id1item.J.JId}), Composite(AA.AuId=${id2}))`
                });
                for (let item of res) {
                    addResult([id1, id1item.J.JId, item.Id, id2], 'id,jid,id,auid');
                }
            }));
        }
        if (id1item.F) {
            for (let fid of id1item.F) ts.push(co(function *() {
                let res = yield makequery({
                    expr: `And(Composite(F.FId=${fid.FId}), Composite(AA.AuId=${id2}))`
                });
                for (let item of res) {
                    addResult([id1, fid.FId, item.Id, id2], 'id,fid,id,auid');
                }
            }));
        }
        if (id1item.AA) {
            for (let auid of id1item.AA) ts.push(co(function *() {
                let res = yield makequery({
                    expr: `And(Composite(AA.AuId=${auid.AuId}), Composite(AA.AuId=${id2}))`
                });
                for (let item of res) {
                    addResult([id1, auid.AuId, item.Id, id2], 'id,auid,id,auid');
                }
            }));
        }
        yield Promise.all(ts);
    });
}

function proc6(id1, id2, addResult) {
    return co(function *() {
        let ts = [];
        let id1item = queryid(id1);
        let aflist = queryaf(id2);
        id1item = yield id1item;
        if (id1item.length === 0) return;
        id1item = id1item[0];
        if (!id1item.AA) return;
        aflist = yield aflist;
        if (aflist.length === 0) return;
        let afids = {};
        for (let afid of aflist) afids[afid] = true;
        for (let aa of id1item.AA) {
            ts.push(co(function *() {
                let af2list = yield queryaf(aa.AuId);
                for (let afid of af2list) {
                    if (afids[afid]) {
                        addResult([id1, aa.AuId, afid, id2], 'id,auid,afid,auid');
                    }
                }
            }));
        }
        yield Promise.all(ts);
    });
}

function proc7(id1, id2, addResult) {
    return co(function *() {
        let ts = [];
        let id2item = yield queryid(id2);
        if (!id2item[0]) return;
        id2item = id2item[0];
        if (!id2item.AA) return;
        for (let aa of id2item.AA) {
            if (aa.AuId === id1) {
                addResult([id1, id2], 'auid,id');
                break;
            }
        }
        let id1list = yield querybyauid(id1);
        let id2fids = {};
        let id2auids = {};
        if (id2item.F) for (let f of id2item.F) {
            id2fids[f.FId] = true;
        }
        if (id2item.AA) for (let aa of id2item.AA) {
            id2auids[aa.AuId] = true;
        }
        for (let item of id1list) {
            if (item.RId) {
                for (let rid of item.RId) {
                    if (rid === id2) {
                        addResult([id1, item.Id, rid], 'auid,id,id');
                    }
                    ts.push(co(function *() {
                        let item = yield queryid(rid);
                        if (!item[0]) return;
                        if (!item[0].RId) return;
                        for (let rid2 of item[0].RId) if (rid2 === id2) {
                            addResult([id1, item.Id, rid, id2], 'auid,id,id,id');
                        }
                    }));
                }
            }
            if (id2item.C && item.C && item.C.CId === id2item.C.CId) {
                addResult([id1, item.Id, item.C.CId, id2], 'auid,id,cid,id');
            }
            if (id2item.J && item.J && item.J.JId === id2item.J.JId) {
                addResult([id1, item.Id, item.J.JId, id2], 'auid,id,jid,id');
            }
            if (item.F) {
                for (let f of item.F) {
                    if (id2fids[f.FId]) {
                        addResult([id1, item.Id, f.FId, id2], 'auid,id,fid,id');
                    }
                }
            }
            if (item.AA) {
                for (let aa of item.AA) {
                    if (id2auids[aa.AuId]) {
                        addResult([id1, item.Id, aa.AuId, id2], 'auid,id,auid,id');
                    }
                }
            }
        }
        yield Promise.all(ts);
    });
}

function proc9(id1, id2, addResult) {
    return co(function *() {
        let ts = [];
        let id2item = queryid(id2);
        let aflist = queryaf(id1);
        id2item = yield id2item;
        if (id2item.length === 0) return;
        id2item = id2item[0];
        if (!id2item.AA) return;
        aflist = yield aflist;
        if (aflist.length === 0) return;
        let afids = {};
        for (let afid of aflist) afids[afid] = true;
        for (let aa of id2item.AA) {
            ts.push(co(function *() {
                let af2list = yield queryaf(aa.AuId);
                for (let afid of af2list) {
                    if (afids[afid]) {
                        addResult([id1, afid, aa.AuId, id2], 'id,auid,afid,auid');
                    }
                }
            }));
        }
        yield Promise.all(ts);
    });
}

function proc10(id1, id2, addResult) {
    return co(function *() {
        let aflist = queryaf(id1);
        let af2list = queryaf(id2);
        aflist = yield aflist;
        if (aflist.length === 0) return;
        af2list = yield af2list;
        if (af2list.length === 0) return;
        let afids = {};
        for (let afid of aflist) afids[afid] = true;
        for (let afid of af2list) {
            if (afids[afid]) {
                addResult([id1, afid, id2], 'auid,afid,auid');
            }
        }
    });
}

function proc11(id1, id2, addResult) {
    return co(function *() {
        let id1list = querybyauid(id1);
        let id2list = querybyauid(id2);
        id1list = yield id1list;
        id2list = yield id2list;
        let ids = {};
        for (let item of id2list) {
            ids[item.Id] = true;
        }
        for (let item of id1list) {
            if (ids[item.Id]) {
                addResult([id1, item.Id, id2], 'auid,id,auid');
            }
            if (item.RId) for (let rid of item.RId) if (ids[rid]) {
                addResult([id1, item.Id, rid, id2], 'auid,id,id,auid');
            }
        }
    });
}
